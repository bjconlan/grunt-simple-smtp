'use strict';

var grunt = require('grunt');
var fs = require('fs');
var simplesmtp = require('simplesmtp');

/*
======== A Handy Little Nodeunit Reference ========
https://github.com/caolan/nodeunit

Test methods:
test.expect(numAssertions)
test.done()
Test assertions:
test.ok(value, [message])
test.equal(actual, expected, [message])
test.notEqual(actual, expected, [message])
test.deepEqual(actual, expected, [message])
test.notDeepEqual(actual, expected, [message])
test.strictEqual(actual, expected, [message])
test.notStrictEqual(actual, expected, [message])
test.throws(block, [error], [message])
test.doesNotThrow(block, [error], [message])
test.ifError(value)
*/

exports.simple_smtp = {

	default_options: function(test) {
		var client = simplesmtp.connect(25);

		test.expect(3);

		client.once('idle', function() {
			test.ok(true);

			client.useEnvelope({
				from: 'test@testing.com',
				to: ['no-reply@furries.com']
			});
		});

		client.on('message', function() {
			test.ok(true);

			fs.createReadStream('test/fixtures/smtp_message.eml').pipe(client);
		});

		client.on('ready', function(res) {
			test.ok(true);
			client.quit();
		});

		client.on('error', function(err) {
			test.ok(false)
		});

		client.on('end', function() {
			test.done();
		});
	}
};
