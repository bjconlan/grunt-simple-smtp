/*
 * Grunt Simple STMP Plugin
 * 
 * @author Benjamin Conlan
 * @since 0.1.0
 *
 * Copyright (c) 2013 Foldr
 * MIT licensed
 */
'use strict';

var simplesmtp = require('simplesmtp');

module.exports = function(grunt) {

	grunt.registerMultiTask('simple_smtp', 'A simple SMTP server', function() {
		var options = this.options({
			host: 'localhost',
			port: 25,
			keepalive: false
		});

		grunt.log.writeln('Starting SMTP server on ' + options.host + ':' + options.port);
		simplesmtp.createSimpleServer(options, function(req) {
			req.pipe(process.stdout);
			req.accept();
		}).listen(options.port, function(err) {
			if (err) {
				grunt.fatal(err);
			}
		});

		if (options.keepalive) {
			this.async();
			grunt.log.write('Listening...\n');
		}
	});
}
