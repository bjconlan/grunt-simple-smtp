# Grunt simple-smtp task

A simple smtp server for mocks and development used to output all smtp traffic to stdout.

## Getting Started
This plugin requires Grunt `0.4.x`

If you haven't used [Grunt](http://gruntjs.com/) before, be sure to check out the [Getting Started](http://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](http://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

```shell
npm install grunt-simple-smtp --save-dev
```
or alternatively in you package.json
```js
  "devDependencies": { "grunt-simple-smtp": "~0.1" }

``` 

Once the plugin has been installed, it may be enabled inside your Gruntfile with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-simple-smtp');
```

## The "simple_smtp" task

### Overview
In your project's Gruntfile, add a section named `simple_smtp` to the data object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
	simple_smtp: {
		options: {
			// Task-specific options go here.
		}
	}
})
```

### Options

see [simplesmtp](https://github.com/andris9/simplesmtp) for more options.

#### options.port
Type: `Number`
Default value: 25

The port in which to host the service on.

#### options.keepalive
Type: `Boolean`
Default value: false

If the task should be run indefinitly.

### Usage Examples

#### Default Options
In this example, the default options are used starting the service on port 25.

```js
grunt.initConfig({
	simple_smtp: {
		options: {},
	},
})
```

#### Custom Options
In this example the port has been configured to listen on an alternative port.

```js
grunt.initConfig({
	simple_smtp: {
		options: {
			port: 465
		}
	}
})
```

## Credits

Fundamentally this task simply wraps the great [simplesmtp](https://github.com/andris9/simplesmtp) library
by Andris Reinman.

## License

Copyright © 2013 Foldr 
Distributed under the MIT License.